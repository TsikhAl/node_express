"use strict";

const express = require('express');
const app = express();
const port = 3000;

const mongoose = require('mongoose');
mongoose.connect('mongodb://AlexTi:Qwert1234@ds223578.mlab.com:23578/blogs');

const Schema = mongoose.Schema;
const blogSchema = new Schema({
    id: String,
    title: String,
    description: String,
    author: String,
    date: String
});
const Blog = mongoose.model('articles', blogSchema);

var db = mongoose.connection;
db.on('error', (console.error.bind(console, 'connection fail')));
db.once('open', () => console.log('connection good'));

//express view engine
app.set('views', './views');
app.set('view engine', 'pug');

// get home page
app.get('/', (req, resp, next) => {
    Blog.find({}, (err, articles) => {

        if (!articles.length) {
            let error = new Error("Something went wrong, there are no articles in the blog.");
            next(error);
        } else {
            resp.send(articles);
        }
    });
});

// get all blogs
app.get('/blogs', (req, resp, next) => {
    Blog.find({}, (err, articles) => {

        if (!articles.length) {
            let error = new Error("Something went wrong, there are no articles in the blog.");
            next(error);
        } else {
            resp.send(articles);
        }
    });
});

// get one blog on id(number) - example: http://localhost:3000/blogs/1
app.get('/blogs/:id', (req, resp, next) => {
    let urlId = req.params.id;

    Blog.find({ id: urlId }, (err, article) => {

        if (!article.length) {
            let error = new Error("No article with the given ID.");
            next(error);
        } else {
            resp.send(article);
        }
    });
});

// post blog
app.post('/blogs', (req, resp, next) => {
    Blog.count({}, (err, c) => {
        let count = c + 1;

        let newArticle = new Blog({
            id: count,
            title: 'Title for new posted article',
            description: 'Description for new posted article',
            author: 'New Posted Author',
            date: 'New Posted Date'
        });

        newArticle.save().then( (articles) => {
            resp.send(articles);
        })
    });
});

// put blog article on id(number)
app.put('/blogs/:id', (req, resp, next) => {
    let urlId = req.params.id;

    Blog.findOneAndUpdate({ id: urlId }, {
        id: urlId,
        title: 'Updated Title',
        description: 'Updated Description',
        author: 'Updated Author',
        date: 'Updated Date'
    },
        () => { }
    );

    Blog.find({ id: urlId }, (err, article) => {

        if (!article.length) {
            let error = new Error("The article wasn't updated.");
            next(error);
        } else {
            resp.send(article);
        }
    });
});

// delete blog article on id(number)
app.delete('/blogs/:id', (req, resp, next) => {
    let urlId = req.params.id;

    Blog.remove({ id: urlId }, () => { });

    Blog.find({}, (err, articles) => {
        resp.send(articles);
    });
});

// error handling middleware
app.use( (err, req, res, next) => {
    res.status(404).send(err.message);
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }

    console.log(`server is listening on ${port}`)
});
